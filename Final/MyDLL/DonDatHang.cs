//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MyDLL
{
    using System;
    using System.Collections.Generic;
    
    public partial class DonDatHang
    {
        public DonDatHang()
        {
            this.ChiTietDonHangs = new HashSet<ChiTietDonHang>();
            this.LichSuMuaHangs = new HashSet<LichSuMuaHang>();
        }
    
        public int ddh_ID { get; set; }
        public string ddh_Address { get; set; }
        public string ddh_Phone { get; set; }
        public string ddh_Email { get; set; }
        public Nullable<decimal> ddh_Total { get; set; }
        public Nullable<System.DateTime> ddh_Date { get; set; }
        public string ddh_UserName { get; set; }
        public string ddh_FullName { get; set; }
    
        public virtual ICollection<ChiTietDonHang> ChiTietDonHangs { get; set; }
        public virtual ICollection<LichSuMuaHang> LichSuMuaHangs { get; set; }
    }
}
