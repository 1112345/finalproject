//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MyDLL
{
    using System;
    using System.Collections.Generic;
    
    public partial class DetailPhone
    {
        public DetailPhone()
        {
            this.Phones = new HashSet<Phone>();
        }
    
        public int De_ID { get; set; }
        public string De_Maker { get; set; }
        public string De_Screen { get; set; }
        public string De_CPU { get; set; }
        public string De_RAM { get; set; }
        public string De_HDH { get; set; }
        public string De_CAM1 { get; set; }
        public string De_CAM2 { get; set; }
        public string De_ROM { get; set; }
        public string De_SD { get; set; }
        public string De_Pin { get; set; }
    
        public virtual ICollection<Phone> Phones { get; set; }
    }
}
