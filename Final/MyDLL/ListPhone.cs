//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MyDLL
{
    using System;
    using System.Collections.Generic;
    
    public partial class ListPhone
    {
        public ListPhone()
        {
            this.Phones = new HashSet<Phone>();
        }
    
        public string lp_Maker { get; set; }
        public Nullable<int> lp_soluong { get; set; }
    
        public virtual ICollection<Phone> Phones { get; set; }
    }
}
