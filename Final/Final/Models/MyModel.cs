﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Final.Models
{
    public class CreateProductModel
    {
        [Required]
        [Display(Name = "Tên sản phẩm")]
        public string NameProduct { get; set; }

        [Required]
        [Display(Name = "Trạng thái")]
        public string Status { get; set; }

        [Required]
        [Display(Name = "Giá")]
        public int Price { get; set; }

        [Required]
        [Display(Name = "Hãng sản xuất")]
        public string Maker { get; set; }

        [Display(Name = "Ảnh sản phẩm")]
        public string Image { get; set; }

    }
}