﻿using MyDLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Final.ViewModels
{
    public class ShoppingCartViewModel
    {
        public List<GioHang> CartItems { get; set; }
        public decimal CartTotal { get; set; }
    }
}