﻿using Final.Models;
using MyDLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Owin.Security;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace Final.Controllers
{
    [Authorize]
    public class CheckoutController : Controller
    {
        ProductEntities storeDB = new ProductEntities();
        const string PromoCode = "FREE";
        //
        // GET: /Checkout/
        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        public ActionResult Index()
        {
            return View();
        }
        //
        // GET: /Checkout/AddressAndPayment
        public ActionResult AddressAndPayment()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            var model = new DonDatHang() { 
                ddh_UserName = user.UserName,
                ddh_Phone = user.PhoneNumber,
                ddh_Email = user.Email,
                ddh_FullName = user.FullName,
                ddh_Address = user.Address
            };

            return View(model);
        }
        //
        // POST: /Checkout/AddressAndPayment
        [HttpPost]
        public ActionResult AddressAndPayment(FormCollection values)
        {
            var order = new DonDatHang();
            TryUpdateModel(order);

            try
            {
                
                order.ddh_UserName = User.Identity.Name;
                order.ddh_Date = DateTime.Now;

                //Save Order
                storeDB.DonDatHangs.Add(order);
                storeDB.SaveChanges();
                //Process the order
                var cart = ShoppingCart.GetCart(this.HttpContext);
                cart.CreateOrder(order);

                return RedirectToAction("Complete",
                    new { id = order.ddh_ID });
                
            }
            catch
            {
                //Invalid - redisplay with errors
                return View(order);
            }
        }
        //
        // GET: /Checkout/Complete
        public ActionResult Complete(int id)
        {
            // Validate customer owns this order
            bool isValid = storeDB.DonDatHangs.Any(
                o => o.ddh_ID == id &&
                o.ddh_UserName == User.Identity.Name);

            if (isValid)
            {
                return View(id);
            }
            else
            {
                return View("Error");
            }
        }
	}
}