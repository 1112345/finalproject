﻿using MyDLL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using PagedList;

namespace Final.Controllers
{
    public class HomeController : Controller
    {
        private ProductEntities db = new ProductEntities();


        public ActionResult Index(int? page, string Maker, string OS, int? Price)
        {
            bool search = false;
            ViewBag.Title = "MobileCenter";
            var phones = from s in db.Phones select s;
            

            if (!String.IsNullOrEmpty(Maker) && !Maker.Equals("All"))
            {
                search = true;
                phones = phones.Where(s => s.p_Maker.ToUpper().Contains(Maker.ToUpper()));
            }
            if (!String.IsNullOrEmpty(OS) && !OS.Equals("All"))
            {
                search = true;
                phones = phones.Where(s => s.DetailPhone.De_HDH.ToUpper().Contains(OS.ToUpper()));
            }
            if (Price != null && Price != 0)
            {
                search = true;
                switch(Price)
                {
                    case 2:
                        phones = phones.Where(s => s.p_Price > 1000000 && s.p_Price < 3000000);
                        break;
                    case 4:
                        phones = phones.Where(s => s.p_Price > 3000000 && s.p_Price < 5000000);
                        break;
                    case 6:
                        phones = phones.Where(s => s.p_Price > 5000000 && s.p_Price < 7000000);
                        break;
                    case 8:
                        phones = phones.Where(s => s.p_Price > 7000000 && s.p_Price < 9000000);
                        break;
                    case 10:
                        phones = phones.Where(s => s.p_Price > 9000000 && s.p_Price < 11000000);
                        break;
                    case 12:
                        phones = phones.Where(s => s.p_Price > 11000000 && s.p_Price < 13000000);
                        break;
                    case 14:
                        phones = phones.Where(s => s.p_Price > 13000000 && s.p_Price < 15000000);
                        break;
                    case 16:
                        phones = phones.Where(s => s.p_Price > 15000000 && s.p_Price < 17000000);
                        break;
                    case 18:
                        phones = phones.Where(s => s.p_Price > 17000000 && s.p_Price < 19000000);
                        break;
                    case 20:
                        phones = phones.Where(s => s.p_Price > 19000000);
                        break;
                    default: break;
                }
            }
            if(search == false)
                phones = phones.Where(s => s.p_hot.Equals(1));
            phones = phones.OrderBy(s => s.p_ID);
            int pageSize = 8;
            int pageNumber = (page ?? 1);
            return View(phones.ToPagedList(pageNumber, pageSize));
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}
