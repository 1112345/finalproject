﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MyDLL;
using System.Web.Mvc.Html;
using System.Web.Helpers;
using PagedList;

namespace Final.Controllers
{
    public class MobileController : Controller
    {
        private ProductEntities db = new ProductEntities();

        // GET: /Mobile/
        public ActionResult Index(int id, int? page)   // id is maker; example 1 is Iphone
        {
            var phones = from s in db.Phones select s;

            ViewBag.currentID = id;

            switch (id)
            {
                case 1:
                    phones = phones.Where(s => s.p_Maker.ToUpper().Contains("iphone".ToUpper()));
                    break;
                case 2:
                    phones = phones.Where(s => s.p_Maker.ToUpper().Contains("ss".ToUpper()));
                    break;
                case 3:
                    phones = phones.Where(s => s.p_Maker.ToUpper().Contains("nokia".ToUpper()));
                    break;
                case 4:
                    phones = phones.Where(s => s.p_Maker.ToUpper().Contains("htc".ToUpper()));
                    break;
                case 5:
                    phones = phones.Where(s => s.p_Maker.ToUpper().Contains("lg".ToUpper()));
                    break;
                case 6:
                    phones = phones.Where(s => s.p_Maker.ToUpper().Contains("oppo".ToUpper()));
                    break;
                default:
                    break;

            }
            phones = phones.OrderBy(s => s.p_ID);
            int pageSize = 8;
            int pageNumber = (page ?? 1);
            return View(phones.ToPagedList(pageNumber, pageSize));
        }

        // GET: /Mobile/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Phone phone = db.Phones.Find(id);
            if (phone == null)
            {
                return HttpNotFound();
            }
            return View(phone);
        }

        // GET: /Mobile/Create
        public ActionResult Create()
        {
            ViewBag.p_DeID = new SelectList(db.DetailPhones, "De_ID", "De_Maker");
            return View();
        }

        // POST: /Mobile/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="p_ID,p_DeID,p_Name,p_Status,p_Price,p_Maker,p_Image,p_hot")] Phone phone)
        {
            if (ModelState.IsValid)
            {
                db.Phones.Add(phone);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.p_DeID = new SelectList(db.DetailPhones, "De_ID", "De_Maker", phone.p_DeID);
            return View(phone);
        }

        // GET: /Mobile/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Phone phone = db.Phones.Find(id);
            if (phone == null)
            {
                return HttpNotFound();
            }
            ViewBag.p_DeID = new SelectList(db.DetailPhones, "De_ID", "De_Maker", phone.p_DeID);
            return View(phone);
        }

        // POST: /Mobile/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="p_ID,p_DeID,p_Name,p_Status,p_Price,p_Maker,p_Image,p_hot")] Phone phone)
        {
            if (ModelState.IsValid)
            {
                db.Entry(phone).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.p_DeID = new SelectList(db.DetailPhones, "De_ID", "De_Maker", phone.p_DeID);
            return View(phone);
        }

        // GET: /Mobile/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Phone phone = db.Phones.Find(id);
            if (phone == null)
            {
                return HttpNotFound();
            }
            return View(phone);
        }

        // POST: /Mobile/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Phone phone = db.Phones.Find(id);
            db.Phones.Remove(phone);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        [HttpPost]
        public ActionResult AddComment(string noidung, int idsp)
        {
            var comment = new BinhLuan()
            {
                cmt_spID = idsp,
                cmt_noidung = noidung
            };
            db.BinhLuans.Add(comment);
            db.SaveChanges();
            return Json(new { success = true });
        }
        public ActionResult DanhSachBinhLuan(int idsp)
        { 
            var listcmt = db.BinhLuans.Where(b => b.cmt_spID == idsp).ToList();
            return Json(listcmt);
        }
        [Authorize(Roles = "Admin")]
        public ActionResult DanhsachSanPham(int? page)
        {
            var phones = from s in db.Phones select s;
            phones = phones.OrderBy(s => s.p_ID);
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(phones.ToPagedList(pageNumber, pageSize));
        }
    }
}
