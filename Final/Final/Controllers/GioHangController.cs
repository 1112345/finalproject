﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MyDLL;
using Final.Models;
using Final.ViewModels;

namespace Final.Controllers
{
    public class GioHangController : Controller
    {
        private ProductEntities storeDB = new ProductEntities();

        // GET: /GioHang/
        public ActionResult Index()
        {
            ViewBag.Title = "MobileCenter";
            var cart = ShoppingCart.GetCart(this.HttpContext);

            // Set up our ViewModel
            var viewModel = new ShoppingCartViewModel
            {
                CartItems = cart.GetCartItems(),
                CartTotal = cart.GetTotal()
            };
            // Return the view
            return View(viewModel);
        }
        //
        // POST: /Store/AddToCart/5
        [HttpPost]
        public ActionResult AddToCart(int id, string action, string controller)
        {
            // Retrieve the album from the database
            var addedPhone = storeDB.Phones
                .Single(phone => phone.p_ID == id);

            // Add it to the shopping cart
            var cart = ShoppingCart.GetCart(this.HttpContext);

            cart.AddToCart(addedPhone);
            // Go back to the main store page for more shopping
            return RedirectToAction(action, controller, new { area = "" });
        }
        //
        // AJAX: //GioHang/UpdateCart
        [HttpPost]
        public ActionResult UpdateCart(int id, int count)
        {
            var cart = ShoppingCart.GetCart(this.HttpContext);

            // Set up our ViewModel
            
            // Get the name of the phone to display confirmation
            string phoneName = storeDB.GioHangs
               .Single(item => item.gh_recordID == id).Phone.p_Name;
            // update from cart
            int itemCount = cart.UpdateQuantity(id,count);

            // Display the confirmation message
            var results = new ShoppingCartRemoveViewModel
            {
                Message = Server.HtmlEncode(phoneName) +
                    " has been updated from your shopping cart.",
                CartTotal = cart.GetTotal(),
                CartCount = cart.GetCount(),
                ItemCount = itemCount,
                DeleteId = id
            };
            return Json(results);
        }
        //
        // AJAX: /GioHang/RemoveFromCart/5
        [HttpPost]
        public ActionResult RemoveFromCart(int id)
        {
            // Remove the item from the cart
            var cart = ShoppingCart.GetCart(this.HttpContext);

            // Get the name of the phone to display confirmation
            string phoneName = storeDB.GioHangs
                .Single(item => item.gh_recordID == id).Phone.p_Name;

            // Remove from cart
            int itemCount = cart.RemoveFromCart(id);

            // Display the confirmation message

            var results = new ShoppingCartRemoveViewModel
            {
                Message = Server.HtmlEncode(phoneName) +
                    " has been removed from your shopping cart.",
                CartTotal = cart.GetTotal(),
                CartCount = cart.GetCount(),
                ItemCount = itemCount,
                DeleteId = id
            };
            return Json(results);
        }
    }
}
